import random, math, time
class Terry():
    def __init__(self, my_position, state , neighbour_coordinates = [], friends = 0):
        self.my_position = my_position
        self.state = state
        self.neighbour_coordinates = neighbour_coordinates
        self.friends = friends


    def find_friends(self, World):
        for i in range(self.my_position[0], World.row_length):
            for j in range(self.my_position[1], World.row_length): 
                length = math.sqrt(((self.my_position[0] - i)**2 + (self.my_position[1] - j)**2))
                if World.world[i][j].state == 1 and length == 1 or length == math.sqrt(2):
                    self.friends += 1
        
        for i in range(0, self.my_position[0]):
            for j in range(0, self.my_position[1]): 
                length = math.sqrt(((self.my_position[0] - i)**2 + (self.my_position[1] - j)**2))
                if World.world[i][j].state == 1 and length == 1 or length == math.sqrt(2):
                    self.friends += 1
        
        if self.friends == 3 and self.state == 0:
            return True
        elif self.friends == 2:
            return True




class World():
    def __init__(self, n):
        self.row_length = n
        self.world = []
        self.populate()


    def populate(self):
        for x in range(self.row_length):
            self.world.append([])
            for y in range(self.row_length):
                state = random.randint(0,1)
                terry = Terry((x,y),state)
                self.world[x].append(terry)

    def show_World(self):
        for x in range(self.row_length):
            print('\n')
            for terry in self.world[x]:
                print(terry.state, end=' ')
        print('\n')

    def game_of_life(self):
        while True:
            new_world=[]
            for x in range(self.row_length):
                new_world.append([])
                for y in range(self.row_length):
                    terry = Terry((x,y),0)
                    new_world[x].append(terry)
                    currentTerry = self.world[x][y]
                    if currentTerry.find_friends(self):
                        terry.state = 1
            self.world = new_world
            self.show_World()
            time.sleep(4)





test = World(20)
test.game_of_life()
