# FizzBuzz

![fizzbuzz picture](../../agile/pairing/images/fizz.png)

FizzBuzz is a counting and number substitution game mainly aimed at teaching children division. Players take turns counting up from one, replacing any number that is divisible by three with the work "Fizz", any number divisible by five with the word "Buzz", and any number divisible by both three and five i replaced with "FizzBuzz"

Remember that the solution needs to be test driven. If you need help with getting started and understanding the TDD workflow, take a look at the [walkthrough](tdd.md) for some inspiration. This walkthough is designed to illustrate an approach to TDD rather than to guide you towards a particular answer to the problem.

## Requirements

We need a backend for the game that can correctly score the game. 

#### 1. Normal Numbers Return the Same Number

_As a game designer_  
_I want a normal number to return that number_  
_So that the game can be scored according to the rules_    

GIVEN I have started a game  
WHEN I enter a number  
THEN the result is returned  

**Test Cases:**

 Number | Result 
---|---
1 | 1
2 | 2
4 | 4


#### 2. Multiples of Three Return Fizz

_As a game designer_  
_I want a multiple of three to return 'Fizz'_  
_So that the game can be scored according to the rules_  

GIVEN I have started the game   
WHEN I enter $number   
THEN the $result is returned  

**Test Cases:**

 Number | Result 
---|---
3 | Fizz
9 | Fizz
123 | Fizz


#### 3. Multiples of Five Return Buzz

_As a game designer_  
_I want a multiple of five to return "Buzz"_
_So that the game can be scored according to the rules_  

GIVEN that I have started a game  
WHEN I enter $number
THEN $result is returned


**Test Cases:**

Number | Result 
---|---
5 | Buzz
20 | Buzz
200 | Buzz

#### 4. Multiples of Three and Five Return FizzBuzz

_As a games designer_  
_I want a multiple of three and five to return 'FizzBuzz'_  
_So that the game can be scored according to the rules_  

GIVEN I have started the game  
WHEN I enter $number  
THEN $result is returned  

**Test Cases:**

Number | Result 
---|---
15 | FizzBuzz
45 | FizzBuzz
315 | FizzBuzz

It turns out that FizzBuzz has lots of different variants that people play in different countries and for different ages of children. One of the most interesting we have seen is Fizz Buzz Pop where the word 'pop' is substituted for numbers that are multiples of seven. We want you to extend the rules of our current FizzBuzz game to allow players to play FizzBuzzPop instead. 

#### 5. Multiples of Seven Return Pop

_As a games designer_  
_I want a multiple of five and seven to return 'buzz pop'_  
_So that the game can be scored according to the rules_  

GIVEN I have started the game  
WHEN I enter $number  
THEN $result is returned  

**Test Cases:**

Number | Result 
---|---
 7 | Pop
28 | Pop
77 | Pop


#### 6. Multiples of Three and Seven Return FizzPop

_As a games designer_  
_I want a multiple of three and seven to return 'fizz pop'_  
_So that the game can be scored according to the rules_  

GIVEN I have started the game  
WHEN I enter $number  
THEN $result is returned  

**Test Cases:**

Number | Result 
---|---
 21 | FizzPop
63 | FizzPop
126 | FizzPop


#### 7. Multiples of Five and Seven Return Buzz Pop

_As a games designer_ 
_I want a multiple of five and seven to return 'BuzzPop'_   
_So that the game can be scored according to the rules_  

GIVEN I have started the game  
WHEN I enter $number  
THEN $result is returned  

**Test Cases:**

Number | Result 
---|---
 35 | BuzzPop
70| BuzzPop
140 | BuzzPop


#### 8. Multiples of Three, Five and Seven Return FizzBuzzPop

_As a games designer_  
_I want a multiple of three, five and seven to return 'fizz buzz pop'_  
_So that the game can be scored according to the rules_  

GIVEN I have started the game  
WHEN I enter $number  
THEN $result is returned  

**Test Cases:**

Number | Result 
---|---
 105 | FizzBuzzPop
210  FizzBuzzPop
315 | FizzBuzzPop


### Walkthrough

The accompanying walk-through for this code is [here](tdd.md)