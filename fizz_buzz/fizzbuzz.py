def fizz_buzz(number):
    if number % 105 == 0:
        return 'FizzBuzzPop'
    if number % 35 == 0:
        return 'BuzzPop'
    if number % 21 == 0:
        return 'FizzPop'
    if number % 15 == 0:
        return 'FizzBuzz'
    if number % 7 == 0:
        return 'Pop'
    if number % 5 == 0:
        return 'Buzz'
    if number % 3 == 0:
        return 'Fizz'
    return number
